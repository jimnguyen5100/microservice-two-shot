import App from "./App";
import { NavLink } from 'react-router-dom'


function HatsList(props) {
  console.log(props.hats)
  return (
      <table className="table table-striped">
          <thead>
            <tr>
              <th>Fabric</th>
              <th>Style Name</th>
              <th>Color</th>
            </tr>
          </thead>
          <tbody>
            {props.hats.map(hat => {
              return(
                  <tr key={hat.import_href}>
                    <td>{ hat.fabric }</td>
                    <td>{ hat.style_name } </td>
                    <td>{ hat.color } </td>
                  </tr>
              );
            })}
          </tbody>
      </table>
  )
}

export default HatsList;