import React from 'react';

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fabric: '',
            styleName: '',
            color: '',
            picture: '',
            locations: []
          };
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleName = this.handleStyleName.bind(this);
        this.handleColor = this.handleColor.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handlePictureChange = this.handlePictureChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        console.log("DATA:::::", data)
        data.style_name = data.styleName
        data.picture_url = data.picture
        delete data.picture
        delete data.styleName
        delete data.locations
        const locationUrl = 'http://localhost:8090/api/hats/';
        console.log("LOCATION::::", locationUrl)
        console.log("DATA2:::::", data)
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        console.log("fetch:",fetchConfig)

        const response = await fetch(locationUrl, fetchConfig);
        console.log("response:",response)

        if (response.ok) {
            const newHat = await response.json();
            console.log("newLocation::", newHat);
            console.log("response:",response)


            const cleared = {
                fabric: '',
                styleName: '',
                color: '',
                locations: '',
                picture: ''
              };
              this.setState(cleared);
        }
    }

    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({ fabric: value })
    }
    handleStyleName(event) {
        const value = event.target.value;
        this.setState({ styleName: value })
    }
    handleColor(event) {
        const value = event.target.value;
        this.setState({ color: value })
    }
    handlePictureChange(event) {
        const value = event.target.value;
        this.setState({ picture: value})
    }
    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({ location: value })
    }

    async componentDidMount() {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        this.setState({ locations: data.locations });

    }
  }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new hat</h1>
                        <form onSubmit={this.handleSubmit} id="create-location-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.fabric} onChange={this.handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.styleName} onChange={this.handleStyleName} placeholder="Style name" required type="text" name="style_name" id="style_name" className="form-control" />
                                <label htmlFor="style_name">Style Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.color} onChange={this.handleColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.picture} onChange={this.handlePictureChange} placeholder="Picture" required type="url" name="picture" id="picture" className="form-control" />
                                <label htmlFor="picture">Picture</label>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.location} onChange={this.handleLocationChange} required name="location" id="location" className="form-select">
                                    <option value="">Choose a location</option>
                                    {this.state.locations.map(location => {
                                        return (
                                            <option key={location.href} value={location.href}>
                                                {location.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default HatForm;