import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from "./ShoesList.js";
import ShoesForm from "./ShoesForm.js";
import HatForm from "./HatForm.js"
import HatsList from "./HatsList.js"



function App(props) {
  if (props.hats === undefined){
    return null
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatsList hats={props.hats}/>}/>
          <Route path="/hats/new" element={<HatForm/>}/>
        <Route path='shoes'>
          <Route path="new" element={<ShoesForm />} />
          <Route path="" element={<ShoesList />} />
        </Route>
        <Route path="/" element={<MainPage />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
