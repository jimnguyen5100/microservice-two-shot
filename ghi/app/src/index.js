import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// import reportWebVitals from './reportWebVitals';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
// reportWebVitals();

async function loadHats() {
  const response = await fetch("http://localhost:8090/api/hats/")
  console.log(response)

  if (response.ok) {

    let data = await response.json()
    console.log(data)

    root.render(
      <React.StrictMode>
        <App hats={data.hats}/>
      </React.StrictMode>
    )
  } else {
    console.error("Response not ok", response)
  }
}
loadHats()

async function loadHats() {
  const response = await fetch("http://localhost:8080/api/shoes/");
  console.log(response);

if (response.ok) {

  let data = await response.json();
  console.log("DATA: ", data);
  root.render(
    <React.StrictMode>
      <App hats={data.shoes}/>
    </React.StrictMode>
  );
}

else {
  console.error("response not ok", response);
}

}

loadHats();
