from django.shortcuts import render
from django.views.decorators.http import require_http_methods

from common.json import ModelEncoder
from .models import Hat, LocationVO
from django.http import JsonResponse
import json
# Create your views here.

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name"]

class HatEncoder(ModelEncoder):
    model = Hat
    properties = [ "style_name", "fabric", "color", "picture_url", "id"]

    encoders = {
        "location": LocationVOEncoder
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET": # List of hats
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats} ,
            encoder=HatEncoder,
            safe=False
        )
    else: #Creating a new hat
        # creating variable
        content = json.loads(request.body)
        try:
            # setting variable to location
            location_href = content["location"]
            print("LOCATION_HREF:", location_href)

            # getting the LocationVO
            location = LocationVO.objects.get(import_href=location_href)
            print("LOCATION:::::::", location)

            # setting a location from value
            content["location"] = location
            print("CONTENT:::::::", content["location"])

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message:" "Invalid location id"},
                status=400,
            )

        #creating a new hat using the info from content
        hat = Hat.objects.create(**content)
        return JsonResponse(hat, encoder=HatEncoder, safe=False)


@require_http_methods(["GET","DELETE"]) # Deleting hat
def api_show_hat(request, pk):
        if request.method == "DELETE":
            count, _ = Hat.objects.filter(id=pk).delete()

            return JsonResponse(
                {"deleted": count >0 }
            )
        else: # Show hat details
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat, encoder=HatEncoder, safe=False
            )

