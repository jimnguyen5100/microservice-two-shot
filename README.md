# Wardrobify

Team: Team Vietnam

* Thinh Mai - Hats
* Jimmy Nguyen - Shoes
* Person 1 - Thinh: Hats
* Person 2 - Jimmy: Shoes

## Design

## Shoes microservice
1.) Create the model.py for the bin to show where it exists within the wardrobe
2.) Create the model.py for Shoes to track the manufacturer, model, color, as well as a picture for the shoe
3.) Create the
4.)
3.) Created views to show the properties of the Bin and Shoe encoders as well as created HTTP methods for getting details regarding bins and shoes
4.) Created RESTful APIs to get a list of shoes, create a new shoe, as well as delete a shoe in insomnia
5.) Worked on poller.py to access the Wadrobe API on my own port 8000. The service poller will be http://wardrobe-api:8000
6.) Made react components to show a list of all shoes and their details
7.) Made react components to show a form to create a new shoe
8.) Provided a way to delete a shoe
## Hats microservice

Models:
-Hat Model: Contains properties for hat. Has a foreignKey for location, enable user to choose which location

-LocationVo: Serves as a link to locations model in wabrobes_api.
----------------------------------------------------------------------------
Views:
-GET(listing hats, and showing hat detail), POST, and Delete built. All functionality works on insomnia

-Used ModelEncoder for Hat and LocationVo encoding
----------------------------------------------------------------------------
URLs:
-Both project urls and app urls are working. Project url includes app url to, app url leads to hats list and specific hat
----------------------------------------------------------------------------
## Hats React

Hat Nav:
-Shows list of hats in tables

Form for creating hat implementented
  -Submit does not work at the moment
  -Delete not yet implemented



